document.documentElement.classList.remove('no-js');

const STORAGE_KEY = 'user-color-scheme';
const toggleButton = document.querySelector('#switch-theme');

const getColorMode = () => getComputedStyle(document.documentElement)
  .getPropertyValue('--color-mode')
  ?.replace(/\"/g, "")
  .trim();

const applySetting = (passedSetting) => {
  let currentSetting = passedSetting || localStorage.getItem(STORAGE_KEY);

  if (currentSetting) {
    document.documentElement.setAttribute('data-user-color-scheme', currentSetting);
    toggleButton.checked = currentSetting === 'dark';
  }
};

const toggleSetting = () => {
  let currentSetting = (localStorage.getItem(STORAGE_KEY) || getColorMode()) === 'dark' ? 'light' : 'dark';
  localStorage.setItem(STORAGE_KEY, currentSetting);

  return currentSetting;
};

toggleButton.addEventListener('change', () => applySetting(toggleSetting()));

applySetting();
