# Loading Animations

[Live demo](https://joluma.gitlab.io/loading-animations)

A showcase of different loading animations, using only css.

## Animations

|||
---|---
Bouncing dots | ![bouncing-dot animation](public/images/bouncing-dot.gif)
Enlarging dots | ![enlarging-dot animation](public/images/enlarging-dot.gif)
Growing bars | ![growing-bar animation](public/images/growing-bar.gif)
Rotating dots | ![rotating-dot animation](public/images/rotating-dot.gif)

## How to run

Just open `public/index.html` in any web browser.

## Uses

- HTML 5
- CSS 3
- CSS variables
